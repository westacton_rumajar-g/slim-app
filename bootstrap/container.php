<?php

$container = $app->getContainer();

//Container untuk View
$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig( __DIR__ . '/../resources/views', [
        'cache' => false,
        'debug' => true
    ]);
    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $container['request']->getUri()->getBasePath()), '/');
    $view->addExtension(new Slim\Views\TwigExtension($container['router'], $basePath));
    $view->getEnvironment()->addGlobal("current_path", $container["request"]->getUri()->getPath());
    $view->addExtension(new \Twig_Extension_Debug());

    // ASSET
    $assetManager = new LoveCoding\TwigAsset\TwigAssetManagement([
        'version' => '3'
    ]);
    $assetManager->addPath('css', '/assets/css');
    $assetManager->addPath('img', '/assets/images');
    $assetManager->addPath('js', '/assets/js');
    $view->addExtension($assetManager->getAssetExtension());

    // Add the validator extension
    $view->addExtension(
        new Awurth\SlimValidation\ValidatorExtension($container['validator'])
    );

    return $view;
};

$capsule = new \Illuminate\Database\Capsule\Manager;
$capsule->addConnection($container['settings']['db']);
$capsule->setAsGlobal();
$capsule->bootEloquent();

//Container untuk database
$container['db'] = function () use ($capsule){
    return $capsule;
};
$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};
$container['csrf'] = function () {
    return new \Slim\Csrf\Guard;
};
$container['validator'] = function () {
    return new Awurth\SlimValidation\Validator();
};