-- MySQL dump 10.13  Distrib 5.7.26, for macos10.14 (x86_64)
--
-- Host: localhost    Database: db_slim
-- ------------------------------------------------------
-- Server version	5.7.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `code` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,NULL,'Test Title','asdf','[\"asdf1\",\"asdf2\"]','2019-07-22 08:51:52','2019-07-22 11:25:38'),(5,NULL,'RIPPLE_KIDS_PARK-30','test','[\"card_input.js (new function)\\r\\n---\\r\\n\\r\\nfunction convertToHalfWidth(card) {\\r\\n    return card.replace(\\/[\\uff01-\\uff5e]\\/g,\\r\\n        function (tmpStr) {\\r\\n            \\/\\/ \\u6587\\u5b57\\u30b3\\u30fc\\u30c9\\u3092\\u30b7\\u30d5\\u30c8\\r\\n            return String.fromCharCode(tmpStr.charCodeAt(0) - 0xFEE0);\\r\\n        });\\r\\n}\\r\\n\\r\\n\\r\\ncard_input.php (line start at 162)\\r\\n---\\r\\n        $(\'#confirm\').on(\'click\', function () {\\r\\n        var $card1=convertToHalfWidth($(\'input[name=\\\"card_1st\\\"]\').val());\\r\\n        var $card2=convertToHalfWidth($(\'input[name=\\\"card_2nd\\\"]\').val());\\r\\n        var $card3=convertToHalfWidth($(\'input[name=\\\"card_3rd\\\"]\').val());\\r\\n        var $card4=convertToHalfWidth($(\'input[name=\\\"card_4th\\\"]\').val());\\r\\n        var $cardsec=convertToHalfWidth($(\'input[name=\\\"sec_code\\\"]\').val());\\r\\n\\r\\n        var $error=0;\\r\\n\\r\\n        \\/\\/ \\u30c8\\u30fc\\u30af\\u30f3\\u53d6\\u5f97\\u524d\\u306b\\u30d0\\u30ea\\u30c7\\u30fc\\u30b7\\u30e7\\u30f3\\r\\n        if($.isNumeric($card1)==false || $.isNumeric($card2)==false || $.isNumeric($card3)==false || $.isNumeric($card4)==false){\\r\\n            $(\'input[name=\\\"card_1st\\\"]\').val($card1.replace(\\/[^0-9]\\/g, \'\'));\\r\\n            $(\'input[name=\\\"card_2nd\\\"]\').val($card2.replace(\\/[^0-9]\\/g, \'\'));\\r\\n            $(\'input[name=\\\"card_3rd\\\"]\').val($card3.replace(\\/[^0-9]\\/g, \'\'));\\r\\n            $(\'input[name=\\\"card_4th\\\"]\').val($card4.replace(\\/[^0-9]\\/g, \'\'));\\r\\n            $(\'.card_number\').css(\'background-color\',\'#ff0000\');\\r\\n            $(\'.card_number_error\').html(\'\\u30ab\\u30fc\\u30c9\\u756a\\u53f7\\u304c\\u6b63\\u3057\\u304f\\u3042\\u308a\\u307e\\u305b\\u3093\');\\r\\n            $error++;\\r\\n        }else{\\r\\n            $(\'.card_number\').css(\'background-color\',\'\');\\r\\n            $(\'.card_number_error\').html(\'\');\\r\\n        }\\r\\n        if($.isNumeric($cardsec)==false){\\r\\n            $(\'input[name=\\\"sec_code\\\"]\').val($cardsec.replace(\\/[^0-9]\\/g, \'\'));\\r\\n            $(\'#sec_code\').css(\'background-color\',\'#ff0000\');\\r\\n            $(\'.sec_code_error\').html(\'\\u30bb\\u30ad\\u30e5\\u30ea\\u30c6\\u30a3\\u30b3\\u30fc\\u30c9\\u304c\\u6b63\\u3057\\u304f\\u3042\\u308a\\u307e\\u305b\\u3093\');\\r\\n            $error++;\\r\\n        }else{\\r\\n            $(\'#sec_code\').css(\'background-color\',\'\');\\r\\n            $(\'.sec_code_error\').html(\'\');\\r\\n        }\"]','2019-07-22 10:58:31','2019-07-22 10:58:31'),(6,NULL,'Test Title','test','[\"asdasdasd\"]','2019-07-22 11:28:25','2019-07-22 11:28:25'),(7,NULL,'Gabby Rodney','test','[\"asdfasdfadf\",\"asdfasdfasdfasdf\"]','2019-07-22 11:32:25','2019-07-22 11:32:25'),(8,1,'Test Gabbs','Gabss','[\"gasbasb\"]','2019-07-22 11:34:29','2019-07-22 11:34:29');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` char(60) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Gabriel Rodin','Rumajar','gabriel.rumajar@gmail.com','$2y$10$C/cyRfLGTg4rKHkjgIoc0OLzIGIkP0y5TlKFCMR4s/mKEHpJketF.','2019-07-19 06:21:38','2019-07-19 06:21:38'),(2,'Gabriel Rodin','Rumajar','gabriel.rumajar@gmail.com','$2y$10$b86.Spb8ksRvKqVdR.LVD.C.9ZFRYEFONm50EgRFI9Y4LMjilQ7zG','2019-07-19 06:41:06','2019-07-19 06:41:06'),(3,'Carbon','Gabriel','rumajar-g@cyscorpions.com','$2y$10$Cq0XSP7nDGnn3AwZODjdMuNPC9yUwE4C/LWL7PBz7hbS1YoFG4Bu2','2019-07-22 08:14:03','2019-07-22 08:14:03'),(4,'Karen','Villamin','karen.villamin@gmail.com','$2y$10$kWPg/TA9cJ50GvVhWEf/ROxL1lnzXawa67DnHU/tbnBdgrdvQ1CE6','2019-07-22 08:16:31','2019-07-22 08:16:31'),(5,'Karen','Villamin','karen.villamin@yahoo.com','$2y$10$USyRYPIPLBI8fZCwGw4Xn.Z7fJZPFui0k7AsS8B4Lp.wWi43ydXTq','2019-07-22 08:17:24','2019-07-22 08:17:24');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-23 10:55:19
