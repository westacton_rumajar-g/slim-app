<?php
require __DIR__.'/../bootstrap/app.php';
require __DIR__.'/../bootstrap/container.php';
require __DIR__.'/../routes/web.php';

$app->run();
