<?php
/*
 * HomeController only for controller sample
 * @hilmanrdn 18-01-2017
 */

namespace App\Controllers;

use App\Models\User;
use Respect\Validation\Validator as V;

class LoginController extends BaseController
{
    public function index($request, $response)
    {
        $this->data['nameKey'] = $this->c->csrf->getTokenNameKey();
        $this->data['valueKey'] = $this->c->csrf->getTokenValueKey();
        $this->data['name'] = $request->getAttribute($this->data['nameKey']);
        $this->data['value'] = $request->getAttribute($this->data['valueKey']);
        $this->data['messages'] = $this->c->flash->getMessages();
        return $this->c->view->render($response, 'login/index.twig', $this->data);
    }

    public function auth($request, $response)
    {
        $user = User::where('email',$request->getParam('email'))->first();
        if (password_verify($request->getParam('password'), $user->password)) {
            $_SESSION['user_id'] = $user->id;
            return $response->withRedirect('/');
        } else {
            $this->c->flash->addMessage('errors', ['cred'=>'Invalid Credentials Please try again']);
            return $response->withRedirect('/login');
        }
    }
}
