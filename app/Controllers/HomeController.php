<?php
/*
 * HomeController only for controller sample
 * @hilmanrdn 18-01-2017
 */

namespace App\Controllers;

class HomeController extends BaseController
{
    public function index($request, $response)
    {
        return $this->c->view->render($response, 'index.twig',$this->data);
    }

    public function logout($request, $response)
    {
        unset($_SESSION['user_id']);
        unset($_SESSION['user']);
        return $response->withRedirect('/login');
    }
}
