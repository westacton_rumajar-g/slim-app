<?php
/*
 * Other Controller that using Container
 * should extends this BaseController
 */

namespace App\Controllers;

use App\Models\User;
use Interop\Container\ContainerInterface;

class BaseController
{
    protected $c;
    protected $data;

    public function __construct(ContainerInterface $container)
    {
        $this->c = $container;
        $this->data['user'] = '';
        if(isset($_SESSION['user_id'])){
            $user = User::getDetails($_SESSION['user_id']);
            $this->data['user'] = $user;
        }
    }
}
