<?php
/*
 * HomeController only for controller sample
 * @hilmanrdn 18-01-2017
 */

namespace App\Controllers;

use App\Models\User;
use Respect\Validation\Validator as V;

class UserController extends BaseController
{
    public function create($request, $response)
    {
        $data['nameKey'] = $this->c->csrf->getTokenNameKey();
        $data['valueKey'] = $this->c->csrf->getTokenValueKey();
        $data['name'] = $request->getAttribute($data['nameKey']);
        $data['value'] = $request->getAttribute($data['valueKey']);
        $data['messages'] = $this->c->flash->getMessages();
        return $this->c->view->render($response, 'create.twig', $data);
    }
    public function signup($request, $response, $args)
    {
        $validator = $this->c->validator->validate($request, [
            'fname'                 => V::length(6, 25),
            'lname'                 => V::length(6,25),
            'email'                 => V::notBlank()->email(),
            'password'              => [
                'rules' => v::length(6, 25),
                'messages' => [
                    'length' => 'This password must have a length between {{minValue}} and {{maxValue}} characters'
                ]
            ],
            'password_confirmation' => [
                'rules' => v::equals($request->getParam('password')),
                'messages' => [
                    'equals' => 'The password confirmation must be equal to the password'
                ]
            ]
        ]);
        if($validator->isValid()){
            User::signup($request->getParsedBody());
            $this->c->flash->addMessage('success', 'Successfully added user');
        }else{
            $errors = $validator->getErrors();
            $this->c->flash->addMessage('errors', $errors);
        }
        return $response->withRedirect('/users/create');
    }
}
