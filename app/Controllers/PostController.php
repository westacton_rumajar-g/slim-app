<?php
/*
 * HomeController only for controller sample
 * @hilmanrdn 18-01-2017
 */

namespace App\Controllers;

use App\Models\Post;
use Respect\Validation\Validator as V;
use App\Models\Validator as Vldt;

class PostController extends BaseController
{
    public function index($request, $response)
    {
        return $this->c->view->render($response, 'post/index.twig');
    }

    public function create($request, $response)
    {
        $this->data['nameKey'] = $this->c->csrf->getTokenNameKey();
        $this->data['valueKey'] = $this->c->csrf->getTokenValueKey();
        $this->data['name'] = $request->getAttribute($this->data['nameKey']);
        $this->data['value'] = $request->getAttribute($this->data['valueKey']);
        $this->data['messages'] = $this->c->flash->getMessages();
        $this->data['posts'] = Post::all();
        return $this->c->view->render($response, 'post/create.twig',$this->data);
    }
    public function store($request, $response)
    {
        $rules = Vldt::validate('post');
        $validator = $this->c->validator->validate($request,$rules);
        if($validator->isValid()){
            Post::store($request->getParsedBody(),$this->data['user']->id);
        }else{
            $errors = $validator->getErrors();
            $this->c->flash->addMessage('errors', $errors);
        }
        return $response->withRedirect('/post/create');
    }

    public function show($request, $response, $args)
    {
        $this->data['posts'] = Post::all();
        $this->data['post'] = Post::find($args['id']);
        $this->data['codes'] = json_decode($this->data['post']->code,true);
        return $this->c->view->render($response, 'post/show.twig',$this->data);
    }

    public function edit($request, $response, $args)
    {
        $this->data['nameKey'] = $this->c->csrf->getTokenNameKey();
        $this->data['valueKey'] = $this->c->csrf->getTokenValueKey();
        $this->data['name'] = $request->getAttribute($this->data['nameKey']);
        $this->data['value'] = $request->getAttribute($this->data['valueKey']);
        $this->data['messages'] = $this->c->flash->getMessages();
        $this->data['posts'] = Post::all();
        $this->data['post'] = Post::find($args['id']);
        $this->data['codes'] = json_decode($this->data['post']->code,true);
        return $this->c->view->render($response, 'post/edit.twig',$this->data);
    }

    public function update($request, $response, $args)
    {
        $rules = Vldt::validate('post');
        $validator = $this->c->validator->validate($request,$rules);
        if($validator->isValid()){
            Post::updatePost($request->getParsedBody(),$args);
        }else{
            $errors = $validator->getErrors();
            $this->c->flash->addMessage('errors', $errors);
        }
        return $response->withRedirect('/post/'.$args['id'].'/edit');
    }

    public function destroy($request, $response, $args)
    {
        Post::deletePost($request->getParsedBody(),$args);
        return $response->withRedirect('/post/create');
    }
}
