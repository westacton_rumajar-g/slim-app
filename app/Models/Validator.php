<?php
/**
 * Created by PhpStorm.
 * User: gabrielrodinrumajar
 * Date: 22/07/2019
 * Time: 6:22 PM
 */

namespace App\Models;

use Respect\Validation\Validator as V;

class Validator
{
    public static function validate($target)
    {
        switch ($target) {
            case 'post':
                $rules = [
                    'title'                 => [
                        'rules' => V::notBlank(),
                        'messages' => [
                            'notBlank' => 'Title must not be blank'
                        ]
                    ],
                    'description'           => [
                        'rules' => V::notBlank(),
                        'messages' => [
                            'notBlank' => 'Description must not be blank'
                        ]
                    ]
                ];
                break;
        }
        return $rules;
    }
}