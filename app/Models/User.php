<?php
/*
 * Sample Model only for Model sample
 * @hilmanrdn 18-01-2017
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = "users";

    public static function emailExists($email)
    {
        $check = self::where('email',$email)->first();
        return !!$check;
    }

    public static function getDetails($id)
    {
        return self::find($id);
    }

    public static function store($params)
    {
        $user = new self;
        $user->firstname = $params['fname'];
        $user->lastname = $params['lname'];
        $user->email = $params['email'];
        $user->password = password_hash($params['password'], PASSWORD_BCRYPT);
        $user->save();
        return true;
    }
}
