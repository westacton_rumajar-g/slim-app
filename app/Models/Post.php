<?php
/**
 * Created by PhpStorm.
 * User: gabrielrodinrumajar
 * Date: 22/07/2019
 * Time: 4:48 PM
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "posts";

    public static function store($params,$user_id)
    {
        $post = new self;
        $post->user_id = $user_id;
        $post->title = $params['title'];
        $post->description = $params['description'];
        $post->code = json_encode($params['code']);
        $post->save();
    }
    public static function updatePost($params,$args)
    {
        $post = self::find($args[id]);
        $post->title = $params['title'];
        $post->description = $params['description'];
        $post->code = json_encode(array_filter($params['code']));
        $post->save();
    }
    public static function deletePost($params,$args)
    {
        self::find($args[id])->delete();
    }
}
