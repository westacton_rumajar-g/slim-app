<?php

namespace App\Middleware;

class Authenticate
{
    public function __invoke($request, $response, $next)
    {
        if(isset($_SESSION['user_id'])){
            return $response->withRedirect('/');
        }
        return $next($request, $response);
    }
}