PHP v7.*

# What is this
Slim with MVC structure like Laravel. Using Twig as template and Eloquent as Database Model.

# Install
donwload project <br>
composer install <br>
import db_slim.sql

# Setting
in bootstrap/app.php set your own database configuration
