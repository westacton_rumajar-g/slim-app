<?php
/*
 * Routing System
 * @hilmanrdn 18-01-2017
 */

use \App\MiddleWare\Authenticate;

$app->add($container->get('csrf'));
$app->get('/', '\App\Controllers\HomeController:index');
$app->get('/logout', '\App\Controllers\HomeController:logout');

$app->group('/', function () use ($app) {
    $app->group('register', function () use ($app) {
        $app->get('', '\App\Controllers\RegisterController:index');
        $app->post('/store', '\App\Controllers\RegisterController:store');
    });
    $app->group('login', function () use ($app) {
        $app->get('', '\App\Controllers\LoginController:index');
        $app->post('/auth', '\App\Controllers\LoginController:auth');
    });
})->add( new Authenticate() );

$app->group('/post', function () use ($app) {
    $app->get('', '\App\Controllers\PostController:index');
    $app->get('/create', '\App\Controllers\PostController:create');
    $app->get('/{id}/show', '\App\Controllers\PostController:show');
    $app->get('/{id}/edit', '\App\Controllers\PostController:edit');
    $app->get('/{id}/destroy', '\App\Controllers\PostController:destroy');
    $app->post('/store', '\App\Controllers\PostController:store');
    $app->post('/{id}/update', '\App\Controllers\PostController:update');
});


